package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/testdata"
)

func TestClient_CreateRelease(t *testing.T) {
	releasedAt, err := ParseDateTime("2019-01-03T01:55:18.203Z")
	require.NoError(t, err)

	baseCRR := CreateReleaseRequest{
		ID:          "projectID",
		Name:        "release name",
		TagName:     "v0.1",
		Description: "release description",
		Ref:         "ref",
		ReleasedAt:  &releasedAt,
		Milestones:  []string{"v1.0"},
		Assets: &Assets{
			Links: []*Link{
				{
					Name:     "hoge",
					URL:      "https://gitlab.example.com/root/awesome-app/-/tags/v0.11.1/binaries/linux-amd64",
					Filepath: "/binaries/linux-amd64",
					LinkType: "other",
				},
			},
		},
	}

	tests := []struct {
		name            string
		res             func() *http.Response
		wantResponse    *CreateReleaseResponse
		wantErrResponse *ErrorResponse
	}{
		{
			name: "success",
			res:  testdata.Responses[testdata.ResponseSuccess],
			wantResponse: func() *CreateReleaseResponse {
				var res CreateReleaseResponse
				err := json.Unmarshal([]byte(testdata.CreateReleaseSuccessResponse), &res)
				require.NoError(t, err)
				return &res
			}(),
		},
		{
			name:            "unauthorized",
			res:             testdata.Responses[testdata.ResponseUnauthorized],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusUnauthorized, Message: "401 Unauthorized"},
		},
		{
			name:            "forbidden",
			res:             testdata.Responses[testdata.ResponseForbidden],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusForbidden, Message: "403 Forbidden"},
		},
		{
			name:            "bad_request",
			res:             testdata.Responses[testdata.ResponseBadRequest],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusBadRequest, Message: "tag_name is missing"},
		},
		{
			name:            "conflict",
			res:             testdata.Responses[testdata.ResponseConflict],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusConflict, Message: "Release already exists"},
		},
		{
			name:            "internal_server_error",
			res:             testdata.Responses[testdata.ResponseInternalError],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Message: "500 Internal Server Error"},
		},
		{
			name: "unexpected_error",
			res: func() *http.Response {
				return &http.Response{
					StatusCode: http.StatusInternalServerError,
					Body:       ioutil.NopCloser(strings.NewReader(testdata.CreateReleaseUnexpectedErrorResponse)),
				}
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Err: "Something went wrong"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mhc := &MockHTTPClient{}
			res := tt.res()
			mhc.On("Do", mock.Anything).Return(res, nil).Once()
			defer require.NoError(t, res.Body.Close())

			gc := &Client{
				baseURL:    "http://127.0.0.1",
				jobToken:   "jobToken",
				projectID:  "projectID",
				httpClient: mhc,
			}
			got, err := gc.CreateRelease(context.Background(), &baseCRR)
			if tt.wantErrResponse != nil {
				require.Error(t, err)
				require.EqualError(t, err, tt.wantErrResponse.Error())

				return
			}

			require.NoError(t, err)
			require.NotNil(t, got)
			mhc.AssertExpectations(t)

			require.Equal(t, baseCRR.Name, got.Name)
			require.Equal(t, baseCRR.TagName, got.TagName)
			require.Equal(t, baseCRR.Description, got.Description)
			require.NotEmpty(t, got.DescriptionHTML)
			require.Len(t, got.Assets.Links, 1)
			require.Equal(t, baseCRR.Assets.Links[0].Name, got.Assets.Links[0].Name)
			require.Equal(t, baseCRR.Assets.Links[0].URL, got.Assets.Links[0].URL)
			require.Contains(t, got.Assets.Links[0].URL, baseCRR.Assets.Links[0].Filepath)
			require.Equal(t, baseCRR.Assets.Links[0].LinkType, got.Assets.Links[0].LinkType)
			require.Equal(t, baseCRR.ReleasedAt.String(), got.ReleasedAt.String())
			require.Len(t, got.Milestones, 1)
			require.Equal(t, baseCRR.Milestones[0], got.Milestones[0].Title)
		})
	}
}

func TestClient_CreateRelease_NonAPIErrors(t *testing.T) {
	tests := []struct {
		name       string
		res        *http.Response
		err        error
		wantErrMsg string
	}{
		{
			name:       "failed_to_call_api",
			err:        fmt.Errorf("something went wrong"),
			wantErrMsg: "failed to do request:",
		},
		{
			name: "not_json_error_response",
			res: &http.Response{
				StatusCode: http.StatusBadRequest,
				Body:       ioutil.NopCloser(strings.NewReader("<not json>")),
			},
			wantErrMsg: "failed to decode error response:",
		},
		{
			name: "not_json_success_response",
			res: &http.Response{
				StatusCode: http.StatusCreated,
				Body:       ioutil.NopCloser(strings.NewReader("<not json>")),
			},
			wantErrMsg: "failed to decode response:",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mhc := &MockHTTPClient{}
			mhc.On("Do", mock.Anything).Return(tt.res, tt.err).Once()
			client, err := New("", "job-token", "", "projectID", mhc)
			require.NoError(t, err)

			res, err := client.CreateRelease(context.Background(), &CreateReleaseRequest{})
			require.Nil(t, res)
			require.Error(t, err)

			require.Contains(t, err.Error(), tt.wantErrMsg)

			if tt.res != nil {
				mhc.AssertExpectations(t)
			}
		})
	}
}
